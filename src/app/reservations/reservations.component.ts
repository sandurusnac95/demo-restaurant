import { ApiService } from './../api.service';
import { Reservation } from './../reservation';
import { CartService } from './../cart.service';
import { Component, OnInit, NgModule, ViewChild } from '@angular/core';
import { Time, DatePipe } from '@angular/common';
import { FormGroup, Validators, FormControl } from '@angular/forms';


@Component({
  templateUrl: './reservations.component.html',
  styleUrls: ['./reservations.component.css'],
  providers: [DatePipe]
})
export class ReservationsComponent implements OnInit {
  constructor(
    public cartService: CartService,
    public datepipe: DatePipe,
    private apiService: ApiService,
  ) { }

  @ViewChild('form') form;

  reservationForm = new FormGroup({
      rname: new FormControl('', Validators.required),
      rpersons: new FormControl('', Validators.required),
      rtime: new FormControl('', Validators.required),
      rdate: new FormControl('', Validators.required),
      includeCart: new FormControl(false, Validators.required),
  });
  rcart = [];
  reservation: Reservation;

  ngOnInit(): void {

  }
  deleteItem(item): void{
    const index = this.cartService.items.indexOf(item);
    if (index !== -1) {
        this.cartService.items.splice(index, 1);
    }
  }

  reservate(): void{
    this.rcart = this.reservationForm.value.includeCart ? this.cartService.items : [];
    // tslint:disable-next-line:max-line-length
    this.reservation = new Reservation(this.reservationForm.value.rdate, this.reservationForm.value.rtime, this.reservationForm.value.rname, this.reservationForm.value.rpersons, this.rcart);
    console.log(this.reservation);
  }

  onSubmit(event): void{
    if (this.reservationForm.valid){
      this.reservate();
      this.apiService.addReservation(this.reservation);
      this.cartService.clearCart();
      event.currentTarget.reset();
      this.reservationForm.reset();
      console.log('Datas OK! Reservation added to the DB!');
    }
    else { console.log('Validation Error!'); }
  }
}
