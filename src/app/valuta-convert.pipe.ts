import { Valuta } from './valuta';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'valutaConvert',
  // pure: false,
})
export class ValutaConvertPipe implements PipeTransform {

  result = '';
  transform(value: number, valuta: Valuta): string {
    console.log('transform' + valuta.name);
    this.result = (value * valuta.raport).toFixed(2).toString();
    switch (valuta.name){
      case 'USD' :
      {
        this.result = '$ ' + this.result;
        break;
      }
      case 'EUR' :
      {
        this.result = '€ ' + this.result;
        break;
      }
      case 'MDL' :
      {
        this.result += ' lei';
        break;
      }
    }
    return this.result;
  }
}
