import { GlobalService } from './../global.service';
import { Valuta } from './../valuta';
import { CartService } from './../cart.service';
import { Item } from '../item';
import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { HttpResponse } from '@angular/common/http';
import { takeUntil, map } from 'rxjs/operators';
import { ReplaySubject } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
    items = Array<Item>();
    valutaAleasa: Valuta;
    private destroyed$: ReplaySubject<boolean> = new ReplaySubject(1);
    constructor(
        private apiService: ApiService,
        private cartService: CartService,
        private globalService: GlobalService
    ) { }

    ngOnInit(): void{
        this.apiService.sendGetRequest().pipe(takeUntil(this.destroyed$)).subscribe((res: HttpResponse<any>) => {
            console.log(res);
            this.items = res.body;
        });
        this.globalService.getValuta().subscribe((val: Valuta) => {
            this.valutaAleasa = val;
            console.log('valuta: ' + this.valutaAleasa.name);
        });
    }

    public firstPage(): void{
        this.items = Array<Item>();
        this.apiService.sendGetRequestToUrl(this.apiService.first).pipe(takeUntil(this.destroyed$)).subscribe((res: HttpResponse<any>) => {
            console.log(res);
            this.items = res.body;
        });
    }
    public previousPage(): void{
        if (this.apiService.prev !== undefined && this.apiService.prev !== '') {
            this.items = Array<Item>();
            // tslint:disable-next-line:max-line-length
            this.apiService.sendGetRequestToUrl(this.apiService.prev).pipe(takeUntil(this.destroyed$)).subscribe((res: HttpResponse<any>) => {
                console.log(res);
                this.items = res.body;
            });
        }
    }
    public nextPage(): void{
        if (this.apiService.next !== undefined && this.apiService.next !== '') {
            this.items = Array<Item>();
            // tslint:disable-next-line:max-line-length
            this.apiService.sendGetRequestToUrl(this.apiService.next).pipe(takeUntil(this.destroyed$)).subscribe((res: HttpResponse<any>) => {
                console.log(res);
                this.items = res.body;
            });
        }
    }
    public lastPage(): void {
        this.items = Array<Item>();
        this.apiService.sendGetRequestToUrl(this.apiService.last).pipe(takeUntil(this.destroyed$)).subscribe((res: HttpResponse<any>) => {
            console.log(res);
            this.items = res.body;
        });
    }

    addToCart(item, qty): void{
        this.cartService.addToCart(item, qty);
    }
}
