import { Time } from '@angular/common';
import { CartItem } from './cart_item';


export class Reservation {
    id: number;
    date: Date;
    time: Time;
    name: string;
    persons: number;
    cart: Array<CartItem>;
    constructor(date: Date, time: Time, name: string, persons: number, cart: Array<CartItem>){
        this.date = date;
        this.time = time;
        this.name = name;
        this.persons = persons;
        this.cart = cart;
    }
}
