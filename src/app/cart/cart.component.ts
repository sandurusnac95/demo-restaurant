import { Valuta } from './../valuta';
import { GlobalService } from './../global.service';
import { CartItem } from './../cart_item';
import { CartService } from './../cart.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  cart = Array<CartItem>();
  total = 0;
  qty = Array<number>();
  valutaAleasa: Valuta;
  constructor(
    private cartService: CartService,
    private globalService: GlobalService,
  ) { }

  ngOnInit(): void {
    this.cart = this.cartService.getItems();
    this.updateTotal();
    this.globalService.getValuta().subscribe((val: Valuta) => {
        this.valutaAleasa = val;
        console.log('valuta: ' + this.valutaAleasa.name);
    });
  }

  addQty(pos): void{
    (pos.qty < 100) ? pos.qty++ : pos.qty = 100;
    this.qty[this.cart.indexOf(pos)] = pos.qty;
    this.updateTotal();
  }
  removeQty(pos): void{
    (pos.qty > 1) ? pos.qty-- : pos.qty = 1;
    this.qty[this.cart.indexOf(pos)] = pos.qty;
    this.updateTotal();
  }
  updateQty(pos): void{
    pos.qty = this.qty[this.cart.indexOf(pos)];
    this.updateTotal();
  }

  updateTotal(): void{
    this.total = 0;
    this.cart.forEach(element => {
      this.total += element.item.price * element.qty;
    });
    this.total = parseFloat(this.total.toFixed(2));
  }

  deleteItem(pos): void{
    const index = this.cart.indexOf(pos);
    if (index !== -1) {
        this.cart.splice(index, 1);
        this.updateTotal();
    }
  }
  clearCart(): void{
    this.cart = this.cartService.clearCart();
    this.updateTotal();
  }
}
