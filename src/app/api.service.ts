import { Valuta } from './valuta';
import { Item } from './item';
import { Reservation } from './reservation';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpErrorResponse, HttpParams, HttpResponse } from '@angular/common/http';

import {  throwError } from 'rxjs';
import { retry, catchError, tap, takeUntil } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class ApiService {

    private SERVER_URL = 'http://localhost:3000';
    constructor(private httpClient: HttpClient) { }

    public first = '';
    public prev = '';
    public next = '';
    public last = '';

    handleError(error: HttpErrorResponse): Observable<HttpErrorResponse> {
        let errorMessage = 'Unknown error!';
        if (error.error instanceof ErrorEvent) {
            errorMessage = `Error: ${error.error.message}`;
        }
        else {
            errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
        }
        window.alert(errorMessage);
        return throwError(errorMessage);
    }

    public sendGetRequest(): Observable<HttpResponse<object> | HttpErrorResponse>{
        // tslint:disable-next-line:max-line-length
        return this.httpClient.get(this.SERVER_URL + '/items', {  params: new HttpParams({fromString: '_page=1&_limit=20'}), observe: 'response'}).pipe(retry(3), catchError(this.handleError), tap(res => {
            console.log(res.headers.get('Link'));
            this.parseLinkHeader(res.headers.get('Link'));
        }));
    }

    public getItem(itemId): Observable<object>{
        return this.httpClient.get(this.SERVER_URL + '/items/' + itemId);
    }

    parseLinkHeader(header): Observable<HttpResponse<object>>{
        if (header.length === 0) {
        return ;
        }

        const parts = header.split(',');
        const links = {};
        parts.forEach( p => {
        const section = p.split(';');
        const url = section[0].replace(/<(.*)>/, '$1').trim();
        const name = section[1].replace(/rel="(.*)"/, '$1').trim();
        links[name] = url;

        });

        // tslint:disable-next-line:no-string-literal
        this.first  = links['first'];
        // tslint:disable-next-line:no-string-literal
        this.last   = links['last'];
        // tslint:disable-next-line:no-string-literal
        this.prev   = links['prev'];
        // tslint:disable-next-line:no-string-literal
        this.next   = links['next'];
    }

    public sendGetRequestToUrl(url: string): Observable<HttpResponse<object> | HttpErrorResponse>{
        return this.httpClient.get(url, { observe: 'response'}).pipe(retry(3),
        catchError(this.handleError), tap(res => {
            console.log(res.headers.get('Link'));
            this.parseLinkHeader(res.headers.get('Link'));
        }));
    }
    public addReservation(reservation: Reservation): void {
        const endPoints = '/reservations';
        this.httpClient.post(this.SERVER_URL + endPoints, reservation).subscribe(data => {
            console.log(data);
        });
    }
    public getValute(): Observable<object>{
        return this.httpClient.get(this.SERVER_URL + '/valute');
    }
}
