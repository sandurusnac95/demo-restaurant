import { GlobalService } from './global.service';
import { HttpResponse } from '@angular/common/http';
import { takeUntil } from 'rxjs/operators';
import { ReplaySubject } from 'rxjs';
import { ApiService } from './api.service';
import { CartService } from './cart.service';
import { Component, OnInit} from '@angular/core';
import { Valuta } from './valuta';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  constructor(
    public cartService: CartService,
    public apiService: ApiService,
    public globalService: GlobalService
  ){}

  private destroyed$: ReplaySubject<boolean> = new ReplaySubject(1);
  title = 'Restaurant';
  valute;
  cartLen = 0;
  valutaAleasa: Valuta;

  ngOnInit(): void {
    this.apiService.getValute().pipe(takeUntil(this.destroyed$)).subscribe((res: HttpResponse<object>) => {
      this.valute = res;
      console.log(this.valute);
      this.valutaAleasa = this.valute[0];
      this.setValuta(this.valutaAleasa);
    });
  }

  setValuta(valuta: Valuta): void{
    this.globalService.setValuta(valuta);
  }
}
