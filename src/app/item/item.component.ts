import { Valuta } from './../valuta';
import { CartService } from './../cart.service';
import { GlobalService } from './../global.service';
import { ReplaySubject } from 'rxjs';
import { Item } from '../item';
import { ApiService } from './../api.service';
import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit {
  constructor(
    private apiService: ApiService,
    private route: ActivatedRoute,
    private cartService: CartService,
    private globalService: GlobalService
  ) { }
  private destroyed$: ReplaySubject<boolean> = new ReplaySubject(1);
  item: Item;
  qty: number;
  valutaAleasa: Valuta;
  ngOnInit(): void {
    this.getItem().pipe(takeUntil(this.destroyed$)).subscribe((res: Item) => {
      console.log(res);
      this.item = res;
    });
    this.qty = 1;
    this.globalService.getValuta().subscribe((val: Valuta) => {
      this.valutaAleasa = val;
      console.log('valuta: ' + this.valutaAleasa.name);
    });
  }

  // tslint:disable-next-line:typedef
  getItem(){
    const itemId = this.route.snapshot.paramMap.get('id');
    return this.apiService.getItem(itemId);
  }

  addQty(): void{
    (this.qty < 100) ? this.qty++ : this.qty = 100;
  }
  removeQty(): void{
    (this.qty > 1) ? this.qty-- : this.qty = 1;
  }
  updateQty(): void{
    if (this.qty < 1) {
      this.qty = 1;
    }
    else if (this.qty > 100){
      this.qty = 100;
    }
  }

  addToCart(): void{
    this.cartService.addToCart(this.item, this.qty);
  }
}
