import { ReservationsComponent } from './reservations/reservations.component';
import { AboutComponent } from './about/about.component';
import { AppComponent } from './app.component';
import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
    { path: '', redirectTo: 'home', pathMatch: 'full'},
    {
      path: 'home' ,
      loadChildren: () => import('./home/home.module').then(m => m.HomeModule)
    },
    { path: 'about' , component: AboutComponent },
    { path: 'reservation' , component: ReservationsComponent },
    {
      path: 'items',
      loadChildren: () => import('./item/item.module').then(m => m.ItemModule)
    },
    {
      path: 'cart',
      loadChildren: () => import('./cart/cart.module').then(m => m.CartModule)
    }
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
