import { catchError } from 'rxjs/operators';
import { BehaviorSubject} from 'rxjs';
import { Valuta } from './valuta';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GlobalService{
  private valutaAleasa: BehaviorSubject<Valuta> = new BehaviorSubject({id: 1, name: 'USD', raport: 1});
  constructor() {}

  setValuta(valuta: Valuta): void{
    this.valutaAleasa.next(valuta);
    console.log('Global Valuta Changed to: ' + valuta.name);
  }

  getValuta(): BehaviorSubject<Valuta>{
    return this.valutaAleasa;
  }
}
