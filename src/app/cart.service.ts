import { CartItem } from './cart_item';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CartService {
  items = Array<CartItem>();
  len = 0;
  constructor() { }

  addToCart(item, qty): void {
    let found = false;
    for (const element of this.items) {
      if (element.item.id === item.id) {
        element.qty += qty;
        found = true;
        break;
      }
    }
    if (!found) {
      this.items.push({['item'] : item, ['qty'] : qty});
    }
  }

  getItems(): CartItem[]{
    return this.items;
  }

  getLength(): number{
    this.len = 0;
    this.items.forEach(element => {
      this.len += element.qty;
    });
    return this.len;
  }

  clearCart(): CartItem[]{
    this.items = [];
    return this.items;
  }
}
