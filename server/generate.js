var faker = require('faker');

var database = { items: []};

for (var i = 1; i<= 150; i++) {
  database.items.push({
    id: i,
    name: faker.commerce.productName(),
    description: faker.lorem.sentence(),
    price: faker.finance.amount(2,50,2),
    imageUrl: faker.image.food(),
  });
}

console.log(JSON.stringify(database));